<?php

    class Career {
        private Joueur $_joueur;
        private Equipe $_equipe;
        private String $_dateDebut;

        public function __construct(Joueur $joueur, Equipe $equipe, String $dateDebut){
            $this->_joueur = $joueur;
            $this->_equipe = $equipe;
            $this->_dateDebut = $dateDebut;
        }

        public function getJoueur() : Joueur{
            return $this->_joueur;
        }

        public function getEquipe() : Equipe{
            return $this->_equipe;
        }

        public function getDateDebut() : String{
            return $this->_dateDebut;
        }

        public function setJoueur(Joueur $joueur) : void{
            $this->_joueur = $joueur;
        }

        public function setEquipe(Equipe $equipe) : void{
            $this->_equipe = $equipe;
        }

        public function setDateDebut(String $dateDebut) : void{
            $this->_dateDebut = $dateDebut;
        }



    }
?>