<?php

    class Equipe {
        private String $_nom;
        private array $_joueurs;
        private Pays $_pays;

        public function __construct(String $nom, Pays $pays){
            $this->_nom = $nom;
            $this->_pays = $pays;
            $this->_joueurs = [];
        }

        public function getNom() : String{
            return $this->_nom;
        }

        public function getJoueurs() : array{
            return $this->_joueurs;
        }

        public function getPays() : Pays{
            return $this->_pays;
        }

        public function setNom(String $nom) : void{
            $this->_nom = $nom;
        }

        public function setPays(Pays $pays) : void{
            $this->_pays = $pays;
        }

        public function addJoueur(Joueur $joueur) : void{
            array_push($this->_joueurs, $joueur);
        }

        public function __toString()
        {
            return $this->_nom;
        }

    }


?>