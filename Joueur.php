<?php   

    class Joueur {
        private String $_nom;
        private String $_prenom;
        private String $_dateNaissance;
        private array $_equipes;
        private Pays $_pays;

        public function __construct(String $nom, String $prenom, String $dateNaissance, Pays $pays){
            $this->_nom = $nom;
            $this->_prenom = $prenom;
            $this->_dateNaissance = $dateNaissance;
            $this->_pays = $pays;
            $this->_equipes = [];
        }

        public function getNom() : String{
            return $this->_nom;
        }

        public function getPrenom() : String{
            return $this->_prenom;
        }

        public function getDateNaissance() : String{
            return $this->_dateNaissance;
        }

        public function getEquipes() : array{
            return $this->_equipes;
        }

        public function getPays() : Pays{
            return $this->_pays;
        }

        public function setNom(String $nom) : void{
            $this->_nom = $nom;
        }

        public function setPrenom(String $prenom) : void{
            $this->_prenom = $prenom;
        }

        public function setDateNaissance(String $dateNaissance) : void{
            $this->_dateNaissance = $dateNaissance;
        }

        public function setEquipes(array $equipes) : void{
            $this->_equipes = $equipes;
        }

        public function setPays(Pays $pays) : void{
            $this->_pays = $pays;
        }

        //add the team and the season to the player 
        public function addEquipe(Equipe $equipe, String $dateDebut) : void{
            $career = new Career($this, $equipe, $dateDebut);
            array_push($this->_equipes, $career);
            $equipe->addJoueur($this);
        }

        public function __toString() : String{
            return $this->_prenom . " " . $this->_nom;
        }

    }


?>