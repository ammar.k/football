
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="wrapper">

    


<?php

spl_autoload_register(function ($class_name) {
    require $class_name . '.php';
});

    $france = new Pays("France");
    $espagne = new Pays("Espagne");
    $italie = new Pays("Italie");
    $allemagne = new Pays("Allemagne");
    $bresil = new Pays("Bresil");
    $argentine = new Pays("Argentine");
    $saudiArabia = new Pays("Saudi Arabia");
    $etatUnis = new Pays("Etat-Unis");
    $portugal = new Pays("Portugal");
    $pologne = new Pays("Pologne");
    $angleterre = new Pays("Angleterre");


    $psg = new Equipe("PSG", $france);
    $strasbourg = new Equipe("Strasbourg", $france);
    $marseille = new Equipe("Marseille", $france);
    $france->addEquipe($psg);
    $france->addEquipe($strasbourg);
    $france->addEquipe($marseille);

    $barcelone = new Equipe("Barcelone", $espagne);
    $realMadrid = new Equipe("Real Madrid", $espagne);
    $seville = new Equipe("Seville", $espagne);
    $espagne->addEquipe($barcelone);
    $espagne->addEquipe($realMadrid);
    $espagne->addEquipe($seville);

    $juventus = new Equipe("Juventus", $italie);
    $milan = new Equipe("Milan", $italie);
    $inter = new Equipe("Inter", $italie);
    $italie->addEquipe($juventus);
    $italie->addEquipe($milan);
    $italie->addEquipe($inter);

    $bayern = new Equipe("Bayern", $allemagne);
    $dortmund = new Equipe("Dortmund", $allemagne);
    $leipzig = new Equipe("Leipzig", $allemagne);
    $allemagne->addEquipe($bayern);
    $allemagne->addEquipe($dortmund);
    $allemagne->addEquipe($leipzig);

    $alhilal = new Equipe("Al-Hilal", $saudiArabia);
    $alnassr = new Equipe("Al-Nassr", $saudiArabia);
    $alittihad = new Equipe("Al-Ittihad", $saudiArabia);
    $saudiArabia->addEquipe($alhilal);
    $saudiArabia->addEquipe($alnassr);
    $saudiArabia->addEquipe($alittihad);

    $losAngeles = new Equipe("Los Angeles", $etatUnis);
    $newYork = new Equipe("New York", $etatUnis);
    $interMiami = new Equipe("Inter Miami", $etatUnis);
    $etatUnis->addEquipe($losAngeles);
    $etatUnis->addEquipe($newYork);
    $etatUnis->addEquipe($interMiami);

    $manUnited = new Equipe("Manchester United", $angleterre);
    $tottenham = new Equipe("Tottenham", $angleterre);

    $mbappe = new Joueur("Mbappe", "Kylian", "20/12/1998", $france);
    $marquinhos = new Joueur("Marquinhos", "Marquinhos", "14/05/1994", $bresil);
    $neymar = new Joueur("Neymar", "Junior", "05/02/1992", $bresil);
    $messi = new Joueur("Messi", "Lionel", "24/06/1987", $argentine);
    $ronaldo = new Joueur("Ronaldo", "Cristiano", "05/02/1985", $portugal);
    $griezmann = new Joueur("Griezmann", "Antoine", "21/03/1991", $france);
    $kempabe = new Joueur("Kempabe", "Antoine", "21/03/1991", $france);
    $lewandowski = new Joueur("Lewandowski", "Robert", "21/03/1991", $pologne);
    $kane = new Joueur("Kane", "Harry", "21/03/1991", $angleterre);
    $coman = new Joueur("Coman", "Kingsley", "21/03/1991", $france);
    $stegen = new Joueur("Stegen", "Marc-André", "21/03/1991", $allemagne);
    $reus = new Joueur("Reus", "Marco", "21/03/1991", $allemagne);

    $mbappe->addEquipe($psg, "2017");
    $marquinhos->addEquipe($psg, "2013");
    $neymar->addEquipe($barcelone, "2013");
    $neymar->addEquipe($psg, "2017");
    $neymar->addEquipe($alhilal, "2023");
    $messi->addEquipe($barcelone, "2004");
    $messi->addEquipe($psg, "2021");
    $messi->addEquipe($interMiami, "2023");
    $ronaldo->addEquipe($realMadrid, "2009");
    $ronaldo->addEquipe($juventus, "2018");
    $ronaldo->addEquipe($manUnited, "2021");
    $ronaldo->addEquipe($alittihad, "2023");
    $griezmann->addEquipe($barcelone, "2019");
    $kempabe->addEquipe($bayern, "2015");
    $lewandowski->addEquipe($dortmund, "2010");
    $lewandowski->addEquipe($bayern, "2014");
    $lewandowski->addEquipe($barcelone, "2022");
    $kane->addEquipe($tottenham, "2009");
    $kane->addEquipe($dortmund, "2010");
    $kane->addEquipe($bayern, "2014");
    $coman->addEquipe($bayern, "2015");
    $stegen->addEquipe($barcelone, "2010");
    $reus->addEquipe($dortmund, "2009");


    echo "<div class='box-pays'>";
    echo "<h3>France</h3>";
    echo "Liste des équipes : <br>";
    foreach($france->getEquipes() as $equipe){
        echo "<p>";
        echo  $equipe;
        echo "</p>";
    }
    echo "</div>";

    echo "<div class='box-pays'>";
    echo "<h3>Espagne</h3>";
    echo "Liste des équipes : <br>";
    foreach($espagne->getEquipes() as $equipe){
        echo "<p>";
        echo  $equipe;
        echo "</p>";
    }
    echo "</div>";

    echo "<div class='box-joueur'>";
    echo "<h3>Messi</h3>";
    echo $messi . " " . $messi->getDateNaissance() . " " . $messi->getPays() . "<br>";
    echo "<h3>career :</h3> <br>";
    foreach($messi->getEquipes() as $career){
        echo "<p>";
        echo $career->getEquipe();
        echo " ";
        echo $career->getDateDebut();
        echo "</p>";
    }
    echo "</div>";

    echo "<div class='box-equipe'>";
    echo "<h3>PSG</h3>";
    echo "Liste des Joueurs : <br>";
        foreach($psg->getJoueurs() as $joueur){
            echo "<p>";
            echo $joueur;
            echo "</p>";
        }
    echo "</div>";

    
    echo "</div>";

?>



</div>

</body>
    <style>
        .wrapper{
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
        }

        .box-pays h3, .box-joueur h3, .box-equipe h3{
            margin: 0;
            padding: 0;
            font-size: 20px;
        }

        .box-equipe{
            width: 200px;
            height: 200px;
            border: 1px solid black;
            background-color: rgb(332,86,66);
            font-size: 12px;
            text-align: center;
            margin: 5px;
            border-radius: 6px;
            cursor: pointer;
        }

        .box-joueur{
            width: 200px;
            height: 200px;
            border: 1px solid black;
            background-color: rgb(282,150,76);
            font-size: 12px;
            text-align: center;
            margin: 5px;
            border-radius: 6px;
            cursor: pointer;
        }

        .box-pays{
            width: 200px;
            height: 200px;
            border: 1px solid black;
            background-color: rgb(212,250,76);
            font-size: 12px;
            text-align: center;
            margin: 5px;
            border-radius: 6px;
            cursor: pointer;
        }


    </style>
</html>