<?php
    class Pays{
        private String $_nom;
        private array $_equipes;

        public function __construct(String $nom){
            $this->_nom = $nom;
            $this->_equipes = [];
        }

        public function getNom() : String{
            return $this->_nom;
        }

        public function getEquipes() : array{
            return $this->_equipes;
        }

        public function setNom(String $nom) : void{
            $this->_nom = $nom;
        }

        public function addEquipe(Equipe $equipe) : void{
            array_push($this->_equipes, $equipe);
        }

        public function __toString()
        {
            return $this->_nom;
        }
    }


?>